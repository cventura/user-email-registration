import { Request, Response, NextFunction } from 'express';
import UserRegistrationService from '../services/UserRegistrationService';
import SendUserEmailService from '../services/SendUserEmailService';

export default class UserController {

  constructor(private userRegistrationService: UserRegistrationService,
              private sendUserEmailService: SendUserEmailService) {}

  generateConfirmationLink = (id: number, code: string) => {
    return `${process.env.LOCAL_URL}/api/users/${id}/${code}`;
  }

  createUser = (request: Request, response: Response, next: NextFunction) => {
    const email = request.body.email;
    const name = request.body.name;

    this.userRegistrationService.registerUser(email, name)
      .then((user) => {
        const confirmationLink = this.generateConfirmationLink(user.id, user.verificationCode);
        // send email service
        return this.sendUserEmailService.sendEmail(user.email, confirmationLink)
          .then(() => {
            return response.json({
              user,
              confirmationLink,
            });
          });
      }).catch((err) => {
        response.json({
          err: err.message,
        });
      });
  }

  confirmUser = (request: Request, response: Response, next: NextFunction) => {
    const userID = request.params.id;
    const confirmationCode = request.params.code;

    this.userRegistrationService.confirmUser(userID, confirmationCode)
      .then((confirmed) => {
        return response.json({
          confirmed,
        });
      })
      .catch((error) => {
        response.status(500).json({
          err: error.message,
        });
      });
  }

}
