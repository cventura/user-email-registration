import { createConnection } from 'typeorm';

export default class DatabaseConnection {

  static async connect() {
    try {
      await createConnection({
        type: 'postgres',
        host: process.env.DB_HOST,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        port: parseInt(process.env.DB_PORT, 10),
        synchronize: true,
        logging: false,
        migrationsTableName: 'app_migrations',
        entities: ['dist/entities/*{.js,.ts}'],
        cli: {
          entitiesDir: 'dist/entities',
        },
      });
      console.log('[+] Connected to the database');
    } catch (e) {
      console.log('[-] Couldn\'t connect to the database.');
      console.log(e);
    }
  }
}
