import Nodemailer, { Transporter } from 'nodemailer';

export default class SendUserEmailService {

  protected transporter: Transporter;

  constructor() {
    this.transporter = Nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: process.env.EMAILER_USER,
        pass: process.env.EMAILER_PASSWORD,
      },
    });
  }

  sendEmail(userEmail: string, userConfirmationLink: string) {
    // TODO: Use a template
    return this.transporter.sendMail({
      from: process.env.EMAILER_USER,
      to: userEmail,
      subject: 'Verification Link',
      html: `<a href='${userConfirmationLink}'>Verify my account</a>`,
    });
  }

}
