import UserRepository from '../repository/UserRepository';
import User from '../entities/User';
import { generateRandomCode, isEmailValid } from '../utils/utils';

export default class UserRegistrationService {

  constructor(private userRepository: UserRepository) {}

  /**
   * Create a new user using his email and name.
   *
   * @param {string} email
   * @param {string} name
   * @returns {Promise<User>}
   * @memberof UserRegistrationService
   */
  registerUser(email: string, name: string): Promise<User> {
    if (!email || !name) {
      throw new Error('Missing email or name');
    }

    if (!isEmailValid(email)) {
      throw new Error('Invalid email address.');
    }

    const user = new User();
    user.createdAt = new Date();
    user.updatedAt = new Date();
    user.email = email;
    user.name = name;
    user.verificationCode = generateRandomCode();
    user.isConfirmed = false;
    return this.userRepository.create(user);
  }

  /**
   * After we send the confirmation link, we need to confirm the user registration
   * via a confirmation code sent to the user's registered email.
   *
   * @param {number} userID
   * @param {string} confirmationToken
   * @returns {Promise<boolean>}
   * @memberof UserRegistrationService
   */
  async confirmUser(userID: number, confirmationToken: string): Promise<boolean> {
    if (!userID || !confirmationToken) {
      throw new Error('Missing user ID or confirmation token.');
    }

    const user = await this.userRepository.getByIDAndToken(userID, confirmationToken);
    if (user) {
      user.isConfirmed = true;
      user.verificationCode = '';
      user.updatedAt = new Date();
      return this.userRepository.update(userID, user);
    }
    throw new Error('User not found');
  }

}
