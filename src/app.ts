import 'reflect-metadata';
import morgan from 'morgan';
import express, { Router } from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import DatabaseConnection from './DatabaseConnection';
import UserController from './controllers/UserController';
import UserRegistrationService from './services/UserRegistrationService';
import SendUserEmailService from './services/SendUserEmailService';
import UserRepository from './repository/UserRepository';
import UserRepositoryImpl from './repository/impl/UserRepositoryImpl';

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: '.env' });

// Create Express server
const app = express();

// Express configuration
app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));

DatabaseConnection.connect().then(() => {
  const router = Router();

  const userRepository: UserRepository = new UserRepositoryImpl();
  const userRegistrationService = new UserRegistrationService(userRepository);
  const sendUserEmailService = new SendUserEmailService();

  const userController = new UserController(userRegistrationService, sendUserEmailService);

  router.post('/users', userController.createUser);
  router.get('/users/:id/:code', userController.confirmUser);
  app.use('/api', router);

});

export default app;
