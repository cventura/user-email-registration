
export const generateRandomCode = () => {
  const code = Math.round(
    Math.random() * Math.pow(10, 4));
  return (code > 1000) ? `${code}` : `0${code}`;
};

export const isEmailValid = (email :string) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};
