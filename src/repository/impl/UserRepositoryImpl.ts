import UserRepository from '../UserRepository';
import { getConnection, getRepository, Repository } from 'typeorm';
import User from '../../entities/User';

export default class UserRepositoryImpl implements UserRepository {

  collection: Repository<User>;

  constructor() {
    this.collection = getConnection().getRepository(User);
  }

  create(user: User): Promise<User> {
    return this.collection.save(user);
  }

  getByIDAndToken(userID: number, code: string): Promise<User> {
    return this.collection.findOne(userID, { where: {
      verificationCode: code,
    }});
  }

  async update(userID: number, user: User): Promise<boolean> {
    try {
      await this.collection.update(userID, user);
      return true;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
}
