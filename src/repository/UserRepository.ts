import User from '../entities/User';

export default interface UserRepository {

  /**
   * Create an user.
   *
   * @param {User} user
   * @returns {Promise<User>}
   * @memberof UserRepository
   */
  create(user: User): Promise<User>;

  /**
   * Get a single user by its ID and token
   *
   * @param {number} userID
   * @returns {Promise<User>}
   * @memberof UserRepository
   */
  getByIDAndToken(userID: number, code: string): Promise<User>;

  /**
   * Update an user model given an ID and a partial user.
   *
   * @param {number} userID
   * @param {User} user
   * @returns {Promise<boolean>}
   * @memberof UserRepository
   */
  update(userID: number, user: User): Promise<boolean>;
}
