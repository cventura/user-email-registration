# User Registration Upstack test

## Features
* NodeJS
* Typescript
* TypeORM (with Postgres)

## Getting Started

Run in development:

First, install dependencies:
```sh
$ yarn install
```
then, copy your .env.example to .env and change it to match your local configuration:
```
$ cp .env.example .env
```

Run the API:
```sh
$ yarn watch-debug
```


## Usage:

- Create an user

`
POST: http://localhost:3000/api/users
`

Params:

- `email`: user email
- `name`: User full name


Check your email for the verification link. Thats all!

## Enhancement:
Because of limited time, I couldn't make a better API and in the future, I'd like to:

- Better exceptions handling.
- An UI (I made just an API).
- Migrations (For now, schema just sync with the database so, USE A TEST DATABASE).
- A routing layer to communicate with controllers.
